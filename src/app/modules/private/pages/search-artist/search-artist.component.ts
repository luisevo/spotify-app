import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { SpotifyService } from '../../services/spotify.service';
import { Artist } from '../../interfaces/album-response.interface';
import { Router } from '@angular/router';

@Component({
  selector: 'app-search-artist',
  templateUrl: './search-artist.component.html',
  styleUrls: ['./search-artist.component.scss']
})
export class SearchArtistComponent {
  formSearch: FormGroup;
  artists: Artist[] = [];

  constructor(
    private fb: FormBuilder,
    private spotify: SpotifyService,
    private router: Router
  ) {
    this.formSearch = this.fb.group({
      q: ['', Validators.required]
    });
  }

  search() {
    if (this.formSearch.valid) {
      this.spotify.searchArtists(this.formSearch.value.q)
        .subscribe(
          res => this.artists = res.artists.items,
          err => console.log(err)
        );
    }
  }

  goArtist(id: string) {
    this.router.navigate(['/spotify/artist-detail', id]);
    // this.router.navigateByUrl('');
  }
}
