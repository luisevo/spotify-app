import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PrivateRoutingModule } from './private-routing.module';
import { PrivateComponent } from './private.component';
import { NewReleasesComponent } from './pages/new-releases/new-releases.component';
import { ArtistDetailComponent } from './pages/artist-detail/artist-detail.component';
import { SearchArtistComponent } from './pages/search-artist/search-artist.component';
import { SharedComponentsModule } from 'src/app/shared/components/components.module';
import { RouterModule } from '@angular/router';
import { ComponentsModule } from './components/components.module';
import { ServicesModule } from './services/services.module';
import { InterceptorsModule } from './interceptors/interceptors.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [
    PrivateComponent,
    NewReleasesComponent,
    ArtistDetailComponent,
    SearchArtistComponent
  ],
  imports: [
    CommonModule,
    PrivateRoutingModule,
    SharedComponentsModule,
    RouterModule,
    ComponentsModule,
    ServicesModule,
    InterceptorsModule,
    ReactiveFormsModule
  ]
})
export class PrivateModule { }
